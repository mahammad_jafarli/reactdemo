import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import SuccessAlert from './SuccessAlert';
import ErrorAlert from './ErrorAlert';

export default class Listing extends Component {

    constructor()
    {
        super();
        this.state= {
            categories: []
        }
    }

    componentDidMount()
    {
        axios.get('http://react-begin:8888/api/category')
            .then(response=>{
                this.setState({categories:response.data});
            });
    }

    onDelete(category_id)
    {
        axios.delete('http://react-begin:8888/api/category/destroy/'+category_id)
            .then(response=>{
                var categories = this.state.categories;

                for(var i =0; i< categories.length; i++)
                {
                    if(categories[i].id==category_id)
                    {
                        categories.splice(i,1);
                        this.setState({categories:categories});
                    }
                }
                this.setState({alert_message:"success"})
                    }).catch(error=>{
                    this.setState({alert_message:"error"})
                })
    }

    render() {
        return (
            <div>

                <hr/>
                {this.state.alert_message=="success"?<SuccessAlert message={"Category deleted successfully"} />:null}
                {this.state.alert_message=="error"?<ErrorAlert message={"Error occured while deleted the category"} />:null}

                <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Updated at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.categories.map(category=>{
                            return (
                                <tr key={category.id}>
                                    <th scope="row">{category.id}</th>
                                    <td>{category.name}</td>
                                    <td>{category.activity==1?("Active"):("Deactive")}</td>
                                    <td>{category.created_at}</td>
                                    <td>{category.updated_at}</td>
                                    <td>
                                        <Link to={'/category/edit/'+category.id}>Edit | </Link>
                                        <a href="#" onClick={this.onDelete.bind(this, category.id)}>Delete</a>
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

