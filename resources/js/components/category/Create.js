import React, { Component } from 'react';
import SuccessAlert from './SuccessAlert';
import ErrorAlert from './ErrorAlert';

export default class Create extends Component {
    constructor()
    {
        super();
        this.onChangeCategoryName = this.onChangeCategoryName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state={
            category_name : '',
            alert_message : '',
        };
    }

    onChangeCategoryName(e) {
        this.setState({
            category_name:e.target.value
        })
    }

    onSubmit(e){
        e.preventDefault();
        const category ={
            category_name : this.state.category_name
        }

        axios.post('http://react-begin:8888/api/category/store', category)
            .then(res=>{
                this.setState({alert_message:"success"})
            }).catch(error=>{
                this.setState({alert_message:"error"})
            });
    }

    render() {
        return (
            <div>
                <hr/>
                {this.state.alert_message=="success"?<SuccessAlert message={"Category created successfully"} />:null}
                {this.state.alert_message=="error"?<ErrorAlert message={"Error occured while creating the category"} />:null}

                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="exampleInputName">Name</label>
                        <input type="text" className="form-control" value={this.state.category_name} onChange={this.onChangeCategoryName} id="exampleInputName" placeholder="Enter name" />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}

