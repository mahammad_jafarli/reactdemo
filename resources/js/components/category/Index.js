import React, { Component } from 'react';
import {Link, Route} from 'react-router-dom';
import Listing from './Listing';
export default class Index extends Component {
    render() {
        return (
            <div>
                <div>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/category" className="btn btn-info">Listing</Link></li>
                            <li className="breadcrumb-item"><Link to="/category/create" className="btn btn-primary">Create</Link></li>
                        </ol>
                    </nav>
                    <Route exact path="/category" component={Listing} />
                </div>
            </div>
        );
    }
}

