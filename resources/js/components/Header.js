import React, { Component } from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Category from './category/Index';
import Create from './category/Create';
import Edit from './category/Edit';
import Error404 from './Error404';


export default class Header extends Component {
    render() {
        return (
                <div>
                    <nav id="navbar-example2" className="navbar navbar-dark bg-dark">
                        <Link className="navbar-brand" to="/">React</Link>
                        <ul className="nav nav-pills">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/about">About</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/category">Category</Link>
                            </li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/category" component={Category} />
                        <Route exact path="/*" component={Error404} />
                        <Route exact path="/category/create" component={Create} />
                        <Route exact path="/category/edit/:id" component={Edit} />
                    </Switch>
                </div>
        );
    }
}

